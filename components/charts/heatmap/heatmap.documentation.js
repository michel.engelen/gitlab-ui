import * as description from './heatmap.md';
import examples from './examples';

export default {
  description,
  examples,
};
