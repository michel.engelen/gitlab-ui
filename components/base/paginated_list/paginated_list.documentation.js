import * as description from './paginated_list.md';
import examples from './examples';

export default {
  description,
  examples,
};
