import SearchBoxByClickDefaultExample from './search_box_by_click.default.example.vue';

export default [
  {
    name: 'Search Box By Click',
    items: [
      {
        id: 'search-box-by-click',
        name: 'default',
        component: SearchBoxByClickDefaultExample,
      },
    ],
  },
];
