# Label

<!-- STORY -->
## Usage
Labels are editable objects that allow users to manually categorize other objects, like issues, merge requests, and epics. They have a name, description, and a customizable color. They provide a quick way to recognize which categories the labeled object belongs to.

**Using the component**
~~~js
<gl-label
  color="dark"
  background-color="#D9C2EE"
  title="Label content"
  description="Some content"
  size="sm"
  tooltipPlacement="top"
  target="http://some.url"
  scoped-labels-documentation-link="http://some.url"
/>
~~~
