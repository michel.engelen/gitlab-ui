import * as description from './avatar.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
};
