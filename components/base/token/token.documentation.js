import examples from './examples';

export default {
  examples,
  events: [
    {
      event: 'close',
      description: 'Emitted when x is clicked',
    },
  ],
};
