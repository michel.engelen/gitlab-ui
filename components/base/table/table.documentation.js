import * as description from './table.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-table',
};
