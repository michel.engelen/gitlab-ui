import BreadcrumbBasicExample from './breadcrumb.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'breadcrumb-basic',
        name: 'Basic',
        description: 'Basic Breadcrumb',
        component: BreadcrumbBasicExample,
      },
    ],
  },
];
